// for generate css file
// const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = {
    entry: './src/App.jsx',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/public'
    },

    devServer:{
        inline: true,
        contentBase: './public',
        port: 9999,
        public: "testapp.ml:9999"
    },

    devtool: "eval-source-map",
    module: {
        loaders : [
            {
                test: /\.jsx?$/,
                exclude: /node-modules/,
                loader: 'babel-loader'
            },
            // {
            //     test: /\.styl$/,
            //     loader: 'style-loader!css-loader!stylus-loader'
            // }
            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader!sass-loader'
            },

            // for generate css file
            // {
            //     test: /\.scss$/,
            //     use: ExtractTextPlugin.extract({
            //     fallback: 'style-loader',
            //     use: ['css-loader', 'sass-loader']
            //     })
            // }
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '*']
    },
    // for generate css file
    // plugins: [
    //     new ExtractTextPlugin('css/style.css')
    // ]
}
